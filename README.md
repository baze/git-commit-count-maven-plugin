# Usage
This Maven plugin provides increasing "version number" calculated from your GIT commits. You can use that number to version your app and distinguish your builds.

Checkout this project http://gitlab.com/baze/git-commit-count-maven-plugin-example to see this Maven plugin in action.


## 1)  Install JAR to your local maven repository:
```
    mvn install:install-file \
        -Dfile=`pwd`/git-commit-count-maven-plugin-1.0.jar \
        -DgroupId=com.lantiainen \
        -DartifactId=git-commit-count-maven-plugin \
        -Dversion=1.0 \
        -Dpackaging=jar
```

## 2) Paste this to your pom.xml to use this plugin:
```
<build>
    <plugins>
        <plugin>
            <groupId>com.lantiainen</groupId>
            <artifactId>git-commit-count-maven-plugin</artifactId>
            <version>1.0</version>

            <executions>
                <execution>
                    <!-- Attach plugin to execute at compile phase -->
                    <phase>compile</phase>
                        <goals>
                            <goal>get-git-push-count</goal>
                        </goals>
                </execution>
            </executions>

            <configuration>
                    <!-- You can provide your GIT directory manually if you want to.
                          Usually this is not needed as the plugin will try to find it from same
                          directory as pom.xml or parent (incase of multimodule project) -->
                    <!-- <gitPath>/path/to/your/git/root/directory/</gitPath> -->
            </configuration>

        </plugin>
    </plugins>
</build>
```

Nwo you can use variable **${git-push-count}** to tag your MANIFEST.MF or use it how ever you want.

## 3) Using counter in MANIFEST.MF 
```
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>3.0.2</version>
            <configuration>
                    <archive>
                            <manifestEntries>
                                    <!-- Add version attribute to MANIFEST.MF and use counter variable from *git-commit-count-maven-plugin* -->
                                    <version>${git-push-count}</version>
                            </manifestEntries>
                    </archive>
            </configuration>
        </plugin>
    </plugins>
</build>
```

