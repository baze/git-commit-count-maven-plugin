package com.lantiainen;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

/**
 * @phase compile
 *
 */
@Mojo(  name = "get-git-push-count")
public class VersionMojo extends AbstractMojo {
	@Component
	private MavenProject project;

	@Parameter(property = "get-git-push-count.gitPath")
	private String gitPath;

	public File findGitRoot() throws NoHeadException {
		File f;
		// If gitPath configured on pom.xml then use that.
		if ( gitPath != null && gitPath.length() > 0) {
			f = new File(gitPath + "/.git/");
			if ( !f.exists() ){
				throw new NoHeadException("Configured Path["+gitPath+"] doesn't have '.git' subdirectory and isn't GIT root.");
			} else {
				return f;
			}
				
		} else {
			// gitPath was not configured in pom.xml. Trying to find out the GIT root.
			f = project.getBasedir();

			File thisDir = new File(f.getPath() + "/.git/");
			if (thisDir.exists()) {
				return thisDir;
			} else {
				File parentDir = new File(f.getParentFile().getPath() + "/.git/");
				if (parentDir.exists()) {
					return parentDir;
				} else {
					throw new NoHeadException("Can't find GIT root. Please provide it as 'gitPath' configuration value.");
				}
			}
		}

	}

	
	public void execute() throws MojoExecutionException {
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		try {
			File gitRoot = findGitRoot();
			getLog().info("Using GitURL[" + gitRoot.getPath() + "]");

			Repository repository = builder.setGitDir(gitRoot).readEnvironment().findGitDir().build();

			Git git = new Git(repository);
			Iterable<RevCommit> log = git.log().call();
			int i = 0;
			for (Iterator<RevCommit> iterator = log.iterator(); iterator.hasNext();) {
				iterator.next();
				i++;
			}
			git.close();

			getLog().info("Setting $git-push-count to value[" + i + "]");
			// Sets the property ${git-push-count} in the project pom
			project.getProperties().setProperty("git-push-count", Integer.toString(i));

		} catch (IOException e) {
			throw new MojoExecutionException("IO error, while reading projects basedir.", e);
		} catch (NoHeadException e) {
			throw new MojoExecutionException( e.getMessage(), e );
		} catch (GitAPIException e) {
			throw new MojoExecutionException("GIT API error.", e);
		}

	}
}